Inspired by [NuGet packages.config is so yesterday](https://www.waitingimpatiently.com/package-reference-nuget/), personally, I wanted to play around with [PackageReference](https://docs.microsoft.com/en-us/nuget/consume-packages/package-references-in-project-files) in order to build and deploy my Sitecore modules successfully based out of various Sitecore versions.

For more information, please visit the following blog posts:

* [How to integrate HubSpot Form to Sitecore](https://buoctrenmay.com/2019/01/21/how-to-integrate-hubspot-form-to-sitecore/)
* [How to test API endpoints from behind a proxy server via Sitecore user interface](https://buoctrenmay.com/2019/03/01/how-to-test-api-endpoints-from-behind-a-proxy-server-via-sitecore-user-interface/)
* [Playing around with Sitecore user-friendly redirect template](https://buoctrenmay.com/2019/03/31/playing-around-with-sitecore-user-friendly-redirect-template/)
* [Playing around with Sitecore custom validators per template](http://buoctrenmay.com/2019/09/02/playing-around-with-sitecore-custom-validators-per-template/)
