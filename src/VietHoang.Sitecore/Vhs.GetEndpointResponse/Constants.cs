﻿namespace Vhs.GetEndpointResponse
{
    public struct Constants
    {
        public struct ConfigPath
        {
            public static string VhsGetEndpointResponse = "vhsGetEndpointResponse/proxyServerSettings";
        }
    }
}