﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GetEndpointResponse.aspx.cs" Inherits="Vhs.GetEndpointResponse.sitecore.admin.GetEndpoint" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title>Get endpoint's response</title>
    <link rel="shortcut icon" href="/sitecore/images/favicon.ico" />
</head>
<body>
    <form id="form1" runat="server">
        <h2>Get Endpoint Response (from behind a proxy server):</h2>
        <p>
            Friendly reminder: please ensure to correct the proxy server settings in "App_Config\Include\z.Vhs.GetEndpointResponse.config" before using. You're able to disable proxy settings if neccessary as well.
        </p>
        <br />
        <div>
            Endpoint URI:
            <br />
            <asp:TextBox runat="server" Width="900px" ID="txtEndpoint" />&nbsp;<asp:Button runat="server" Text="Get Response" ID="btnGetResponse" />
        </div>
        <div>
            <asp:RequiredFieldValidator runat="server" ForeColor="Red" Display="Dynamic" ControlToValidate="txtEndpoint" EnableClientScript="true" ErrorMessage="The uri cannot be empty" />
            <asp:RegularExpressionValidator runat="server" ForeColor="Red" Display="Dynamic" ControlToValidate="txtEndpoint" ErrorMessage="Wrong endpoint format" ValidationExpression="(http|ftp|https):\/\/[\w\-]+\.[\w\-]+.*" />
        </div>
        <br />
        <br />
        <asp:Label Visible="false" runat="server" ID="lblOutput" />
        <br />
        <br />
        <asp:Literal Visible="false" runat="server" ID="ltrOutput" />
    </form>
</body>
</html>
