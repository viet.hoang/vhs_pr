﻿using System;
using System.Net;
using System.Text;
using Sitecore.Configuration;
using Vhs.GetEndpointResponse.Infrastructure;

namespace Vhs.GetEndpointResponse.sitecore.admin
{
    public partial class GetEndpoint : Sitecore.sitecore.admin.AdminPage
    {
        protected override void OnInit(EventArgs e)
        {
            CheckSecurity();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) return;

            var endpointUri = txtEndpoint.Text;
            var response = string.Empty;

            var proxyServerSettings = (ProxyServerSettings)Factory.CreateObject(Constants.ConfigPath.VhsGetEndpointResponse, true);
            using (WebClient wc = new WebClient() { Encoding = Encoding.UTF8 })
            {
                ServicePointManager.ServerCertificateValidationCallback += (cSender, cert, chain, sslPolicyErrors) => true;
                ServicePointManager.ServerCertificateValidationCallback = ((cSender, certificate, chain, sslPolicyErrors) => true);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                bool.TryParse(proxyServerSettings.Enabled, out bool proxyServerEnabled);
                if (proxyServerEnabled)
                {
                    var addr = proxyServerSettings.HostName;
                    int.TryParse(proxyServerSettings.Port, out int port);
                    var username = proxyServerSettings.UserName;
                    var pwd = proxyServerSettings.Password;

                    response = string.Concat(addr, port, username, pwd);
                    WebProxy wp = new WebProxy(addr, port);
                    if (!string.IsNullOrWhiteSpace(username))
                    {
                        wp.Credentials = new NetworkCredential(username, pwd);
                        wc.Proxy = wp;
                    }
                }

                try
                {
                    response = wc.DownloadString(txtEndpoint.Text);
                }
                catch (Exception ex)
                {
                    response = string.Concat(ex.Message, Server.HtmlDecode("<br />"), ex.StackTrace);
                }
            }

            lblOutput.Text = string.Concat(Server.HtmlDecode("<hr />"),
                                            string.Format("Please find out the response of the endpoint \"{0}\" below:", txtEndpoint.Text),
                                            Server.HtmlDecode("<hr />"));
            lblOutput.Visible = true;

            ltrOutput.Text = response;
            ltrOutput.Visible = true;
        }
    }
}