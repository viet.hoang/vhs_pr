﻿namespace Vhs.GetEndpointResponse.Infrastructure
{
    public class ProxyServerSettings
    {
        public string Enabled { get; set; }
        public string HostName { get; set; }
        public string Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}