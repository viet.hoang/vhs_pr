﻿namespace Vhs.CustomValidatorsByTemplate
{
    public static class Templates
    {
        public struct PairSetting
        {
            public const string Id = "{61BFCB9F-A4F9-4B26-89DE-81395584CCF2}";
            public struct Fields
            {
                public const string ScTemplateId = "{A714AF1F-D6E0-45D1-B533-0E3D609967C9}";
                public const string RuntimeValue = "{A5802B40-B63B-4B80-86AA-123D8C8D4C4D}";
            }
        }
    }
}