﻿namespace Vhs.CustomValidatorsByTemplate.Extensions
{
    public static class StringExtensions
    {
        #region HasText
        public static bool HasText(this string source)
        {
            return !string.IsNullOrWhiteSpace(source);
        }
        #endregion
    }

}