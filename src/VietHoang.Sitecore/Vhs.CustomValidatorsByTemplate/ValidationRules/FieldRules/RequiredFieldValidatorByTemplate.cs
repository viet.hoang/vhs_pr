using System;
using System.Linq;
using System.Runtime.Serialization;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Validators;
using Vhs.CustomValidatorsByTemplate;
using Vhs.CustomValidatorsByTemplate.Extensions;

namespace Vhs.CustomValidatorsByTemplate.ValidationRules.FieldRules
{
    [Serializable]
    public class RequiredFieldValidatorByTemplate : StandardValidator
    {
        public override string Name => "Required";

        public RequiredFieldValidatorByTemplate()
        {
        }

        public RequiredFieldValidatorByTemplate(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        protected override ValidatorResult Evaluate()
        {
            var result = base.Parameters["Result"];
            var validatorResult = result != null
                ? (ValidatorResult)Enum.Parse(typeof(ValidatorResult), result)
                : ValidatorResult.FatalError;
            validatorResult = validatorResult == ValidatorResult.Unknown ? validatorResult : ValidatorResult.FatalError;
            var @settingRootId = StringUtil.GetString(Parameters["SettingRootId"], string.Empty);
            if (!@settingRootId.HasText())
            {
                return ValidatorResult.Valid;
            }

            var settingRootItem = this.GetItem().Database.GetItem(@settingRootId);
            if (settingRootItem == null)
            {
                return ValidatorResult.Valid;
            }

            var temlateId = this.GetItem().TemplateID.ToString();
            var settingItems = settingRootItem.GetChildren().InnerChildren;
            var settingItem = settingItems.FirstOrDefault(s => s[new ID(Templates.PairSetting.Fields.ScTemplateId)] == temlateId);
            if (settingItem == null)
            {
                return ValidatorResult.Valid;
            }

            var @required = MainUtil.GetInt(settingItem[new ID(Templates.PairSetting.Fields.RuntimeValue)], 0);
            if (@required == 0)
            {
                return ValidatorResult.Valid;
            }

            var controlValidationValue = base.ControlValidationValue;
            if (controlValidationValue.HasText())
            {
                return ValidatorResult.Valid;
            }

            Text = GetText("The field \"{0}\" is required.", GetFieldDisplayName());
            return GetFailedResult(validatorResult);
        }

        protected override ValidatorResult GetMaxValidatorResult()
        {
            return GetFailedResult(ValidatorResult.FatalError);
        }
    }
}