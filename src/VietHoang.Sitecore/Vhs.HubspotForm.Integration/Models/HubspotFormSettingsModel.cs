﻿namespace Vhs.HubspotForm.Integration.Models
{
    public class HubspotFormSettingsModel
    {
        public string HubspotPortalId { get; set; }

        public string HubspotFormId { get; set; }

        public string HubspotFormApi { get; set; }

        public string InlineCss { get; set; }
    }
}