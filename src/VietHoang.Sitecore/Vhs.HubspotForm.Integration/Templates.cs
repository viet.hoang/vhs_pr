﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vhs.HubspotForm.Integration
{
    public static class Templates
    {
        public static class HubspotFormSettings
        {
            public const string Id = "{E00E44C1-95DA-44FE-A868-B1AF8AD58213}";

            public static class Fields
            {
                public const string HubspotPortalId = "{DA02F6AC-7CBD-467F-913D-AFF5AC3F8BA7}";
                public const string HubspotFormId = "{064A972B-56EE-450F-8306-B38CA8DDFD90}";
                public const string HubspotFormApi = "{76976151-2F4D-4A9E-A930-831E1EDF73BD}";
                public const string InlineCss = "{840FB0A3-0BDB-4953-B79C-0FDEB8047933}";
            }
        }
    }
}