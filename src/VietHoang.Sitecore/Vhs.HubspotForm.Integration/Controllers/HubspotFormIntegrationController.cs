﻿using System.Web.Mvc;
using Sitecore.Mvc.Controllers;
using Sitecore.Mvc.Presentation;
using Vhs.HubspotForm.Integration.Models;

namespace Vhs.HubspotForm.Integration.Controllers
{
    public class HubspotFormIntegrationController : SitecoreController
    {
        public ActionResult HubspotForm()
        {
            var dataSourceId = RenderingContext.CurrentOrNull.Rendering.DataSource;
            var dataSource = Sitecore.Context.Database.Items.GetItem(dataSourceId);
            if (dataSource == null)
            {
                return View();
            }

            var hubspotFormSettings = new HubspotFormSettingsModel
            {
                HubspotPortalId = dataSource[Templates.HubspotFormSettings.Fields.HubspotPortalId],
                HubspotFormId = dataSource[Templates.HubspotFormSettings.Fields.HubspotFormId],
                HubspotFormApi = dataSource[Templates.HubspotFormSettings.Fields.HubspotFormApi],
                InlineCss = dataSource[Templates.HubspotFormSettings.Fields.InlineCss]
            };
            return View(hubspotFormSettings);
        }
    }
}