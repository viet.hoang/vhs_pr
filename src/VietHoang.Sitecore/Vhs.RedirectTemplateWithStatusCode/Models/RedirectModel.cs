﻿namespace Vhs.RedirectTemplateWithStatusCode
{
    public class RedirectModel
    {
        public string RedirectLink { get; set; }

        public int RedirectStatusCode { get; set; }

    }
}