﻿using System.Web.Mvc;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Mvc.Controllers;
using Sitecore.Mvc.Presentation;
using Vhs.RedirectTemplateWithStatusCode.Extensions;

namespace Vhs.RedirectTemplateWithStatusCode.Controllers
{
    public class RedirectController : SitecoreController
    {
        private const int DefaultRedirectStatusCode = 301;

        public ActionResult RedirectWithStatusCode()
        {
            Item redirectItem = Sitecore.Context.Item;
            var dataSourceId = RenderingContext.CurrentOrNull.Rendering.DataSource;
            var dataSource = Sitecore.Context.Database.Items.GetItem(dataSourceId);
            if (dataSource != null)
            {
                redirectItem = dataSource;
            }

            if(redirectItem == null)
            {
                return View();
            }

            if (!int.TryParse(redirectItem[Templates.Redirect.Fields.RedirectStatusCode], out int statusCode))
            {
                statusCode = DefaultRedirectStatusCode;
            }

            if (statusCode <= 0)
            {
                statusCode = DefaultRedirectStatusCode;
            }

            var model = new RedirectModel
            {
                RedirectLink = redirectItem.LinkFieldUrl(new ID(Templates.Redirect.Fields.RedirectLink)),
                RedirectStatusCode = statusCode
            };

            return View(model);
        }
    }
}