﻿namespace Vhs.RedirectTemplateWithStatusCode
{
    public static class Templates
    {
        public static class Redirect
        {
            public const string Id = "{0EA725D1-C293-4324-A84B-88ADDCCDB723}";

            public static class Fields
            {
                public const string RedirectLink = "{101922BE-BF53-4D5A-91B9-145223AD2C0F}";
                public const string RedirectStatusCode = "{0E963594-2642-44F5-A7F0-1C46A88F8343}";
            }
        }
    }
}